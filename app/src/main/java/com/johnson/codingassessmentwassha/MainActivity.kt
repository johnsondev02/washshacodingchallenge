package com.johnson.codingassessmentwassha

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.ProgressBar
import android.widget.TextView
import android.widget.Toast
import androidx.lifecycle.ViewModelProvider
import com.johnson.codingassessmentwassha.data.util.Resource
import com.johnson.codingassessmentwassha.databinding.ActivityMainBinding
import com.johnson.codingassessmentwassha.presentation.viewmodel.JokeViewModel
import com.johnson.codingassessmentwassha.presentation.viewmodel.JokeViewModelFactory
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject

@AndroidEntryPoint
class MainActivity : AppCompatActivity() {
    @Inject
    lateinit var factory: JokeViewModelFactory
    lateinit var viewModel: JokeViewModel
    private lateinit var binding : ActivityMainBinding
    private lateinit var jokeText:TextView
    private lateinit var txtLength : TextView
    private lateinit var txtWords : TextView
    private lateinit var dataSource: TextView
    private lateinit var btnFetchJoke : Button
    private lateinit var progressBar : ProgressBar
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        initView()
        setContentView(binding.root)

        viewModel = ViewModelProvider(this,factory)
            .get(JokeViewModel::class.java)
        viewJoke()

        btnFetchJoke.setOnClickListener {
            viewJoke()
        }
    }

    private fun initView() {
        jokeText = binding.txtjoke
        btnFetchJoke = binding.btnfetchjoke
        progressBar = binding.progressBar
        txtLength = binding.txtjokelength
        txtWords = binding.txtjokewordscount
        dataSource = binding.txtdatasrc
    }


    private fun viewJoke() {
        viewModel.fetchJoke()
        viewModel.joke.observe(this) { response ->
            when (response) {
                is Resource.Success -> {
                    hideProgressBar()
                    response.data?.let {
                        txtWords.text = it.joke?.let { it1 -> countWords(it1) }
                        txtLength.text = it.joke?.let { it1 -> calculateLength(joke = it1) }
                        jokeText.text = it.joke
                        btnFetchJoke.text = "Update Joke"
                    }
                }

                is Resource.Error -> {
                    hideProgressBar()
                    jokeText.text = response.data?.joke
                    txtWords.text = response.data?.joke?.let { it1 -> countWords(it1) }
                    txtLength.text = response.data?.joke?.let { it1 -> calculateLength(joke = it1) }
                    response.message?.let {
                        btnFetchJoke.text = "Update Joke"
                        dataSource.text = it
                    }
                }

                is Resource.Loading -> {
                    showProgressBar()
                    btnFetchJoke.text = "Fetching..."
                }
            }
        }
    }

    private fun countWords(inputString: String): String {
        val words = inputString.split("\\s+".toRegex())
        return "Words: ${words.size}"
    }

    private fun calculateLength(joke:String): String{
        return "Length: ${joke.length}"
    }
    private fun showProgressBar(){
        binding.progressBar.visibility = View.VISIBLE
    }
    private fun hideProgressBar(){
        binding.progressBar.visibility = View.GONE
    }

}