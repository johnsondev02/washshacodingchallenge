package com.johnson.codingassessmentwassha.data.db

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import com.johnson.codingassessmentwassha.data.model.APIResponse


@Database(entities = [APIResponse::class], version = 1)
@TypeConverters(FlagsConverter::class)
abstract class JokeDatabase : RoomDatabase() {
    abstract fun jokeDao(): JokeDao?
    companion object {
        private var instance: JokeDatabase? = null

        fun getDatabase(context: Context): JokeDatabase {
            return instance ?: synchronized(this) {
                instance ?: buildDatabase(context).also { instance = it }
            }
        }

        private fun buildDatabase(context: Context): JokeDatabase {
            return Room.databaseBuilder(
                context.applicationContext,
                JokeDatabase::class.java,
                "joke_database"
            ).build()
        }
    }
}