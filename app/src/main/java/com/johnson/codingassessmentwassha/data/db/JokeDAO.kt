package com.johnson.codingassessmentwassha.data.db

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.johnson.codingassessmentwassha.data.model.APIResponse

@Dao
interface JokeDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertJoke(apiResponse: APIResponse)

    @Query("SELECT * FROM api_responses ORDER BY id DESC LIMIT 1")
    suspend fun getLatestJoke(): APIResponse?
}