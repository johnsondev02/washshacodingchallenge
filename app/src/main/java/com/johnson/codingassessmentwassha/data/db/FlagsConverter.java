package com.johnson.codingassessmentwassha.data.db;

import androidx.room.TypeConverter;

import com.google.gson.Gson;
import com.johnson.codingassessmentwassha.data.model.APIResponse;

public class FlagsConverter {
    private static final Gson gson = new Gson();

    @TypeConverter
    public static String flagsToString(APIResponse.Flags flags) {
        return gson.toJson(flags);
    }

    @TypeConverter
    public static APIResponse.Flags stringToFlags(String flagsString) {
        return gson.fromJson(flagsString, APIResponse.Flags.class);
    }
}
