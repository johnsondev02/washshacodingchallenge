package com.johnson.codingassessmentwassha.data.model

import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.annotations.SerializedName
@Entity(tableName = "api_responses")
data class APIResponse(
    @PrimaryKey(autoGenerate = true) val id: Int = 0,
    val category: String?,
    val error: Boolean,
    val flags: Flags?,
    val apiId: Int?,
    val joke: String?,
    val lang: String?,
    val safe: Boolean,
    val type: String?
) {
    data class Flags(
        val explicit: Boolean,
        val nsfw: Boolean,
        val political: Boolean,
        val racist: Boolean,
        val religious: Boolean,
        val sexist: Boolean
    )
}