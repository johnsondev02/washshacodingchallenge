package com.johnson.codingassessmentwassha.data.repository.dataSourceImpl

import com.johnson.codingassessmentwassha.data.api.JokeApiService
import com.johnson.codingassessmentwassha.data.model.APIResponse
import com.johnson.codingassessmentwassha.data.repository.dataSource.JokeRemoteDataSource
import retrofit2.Response

class JokeRemoteDataSourceImpl(
    private val jokeApiService: JokeApiService
):JokeRemoteDataSource {
    override suspend fun fetchJoke(): Response<APIResponse> {
        return jokeApiService.getJoke()
    }
}