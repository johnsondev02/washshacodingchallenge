package com.johnson.codingassessmentwassha.data.repository.dataSource

import com.johnson.codingassessmentwassha.data.model.APIResponse
import retrofit2.Response

interface JokeRemoteDataSource {
    suspend fun fetchJoke():Response<APIResponse>
}