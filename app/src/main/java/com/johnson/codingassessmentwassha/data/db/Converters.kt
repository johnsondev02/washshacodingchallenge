package com.johnson.codingassessmentwassha.data.db

import androidx.room.TypeConverter
import com.johnson.codingassessmentwassha.data.model.APIResponse

class Converters {
    @TypeConverter
    fun fromApiResponse(api:APIResponse):String{
        return api.joke.toString()
    }

    @TypeConverter
    fun toApiResponse(joke:String):String{
        return joke
    }
}