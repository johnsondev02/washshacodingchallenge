package com.johnson.codingassessmentwassha.data.api

import com.johnson.codingassessmentwassha.data.model.APIResponse
import retrofit2.Response
import retrofit2.http.GET

interface JokeApiService {
    @GET("joke/Programming?type=single ")
    suspend fun getJoke(): Response<APIResponse>
}