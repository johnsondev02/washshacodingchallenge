package com.johnson.codingassessmentwassha.data.repository

import com.johnson.codingassessmentwassha.data.db.JokeDao
import com.johnson.codingassessmentwassha.data.model.APIResponse
import com.johnson.codingassessmentwassha.data.repository.dataSource.JokeRemoteDataSource
import com.johnson.codingassessmentwassha.data.util.Resource
import com.johnson.codingassessmentwassha.domain.repository.JokeRepository
import retrofit2.Response

class JokeRepositoryImpl(
    private val jokeRemoteDataSource: JokeRemoteDataSource,
    private val jokeDao: JokeDao

):JokeRepository {
    override suspend fun fetchJokes(): Resource<APIResponse> {
        return responseToResource(jokeRemoteDataSource.fetchJoke())
    }

    override suspend fun saveJoke(joke: APIResponse) {
        jokeDao.insertJoke(joke)
    }

    override suspend fun getLatestJoke(): APIResponse? {
        return jokeDao.getLatestJoke()
    }

    private fun responseToResource(response: Response<APIResponse>):Resource<APIResponse>{
        if(response.isSuccessful){
            response.body()?.let {result ->
                return Resource.Success(result)
            }
        }
        return Resource.Error(response.message())
    }

}