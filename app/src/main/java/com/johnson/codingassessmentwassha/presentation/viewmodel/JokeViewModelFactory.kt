package com.johnson.codingassessmentwassha.presentation.viewmodel

import android.app.Application
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.johnson.codingassessmentwassha.data.db.JokeDao
import com.johnson.codingassessmentwassha.domain.repository.JokeRepository
import com.johnson.codingassessmentwassha.domain.usecase.FetchJokeUseCase

class JokeViewModelFactory(
    private val app: Application,
    private val fetchJokeUseCase: FetchJokeUseCase,
    private val jokeRepository: JokeRepository
) :ViewModelProvider.Factory{
    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        return JokeViewModel(
            app,
            fetchJokeUseCase,
            jokeRepository
        ) as T
    }
}