package com.johnson.codingassessmentwassha.presentation.di

import com.johnson.codingassessmentwassha.data.api.JokeApiService
import com.johnson.codingassessmentwassha.data.repository.dataSource.JokeRemoteDataSource
import com.johnson.codingassessmentwassha.data.repository.dataSourceImpl.JokeRemoteDataSourceImpl
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
class RemoteDataModule {

    @Provides
    @Singleton
    fun provideJokeRemoteDataSource(jokeApiService: JokeApiService):JokeRemoteDataSource{
        return JokeRemoteDataSourceImpl(jokeApiService)
    }
}