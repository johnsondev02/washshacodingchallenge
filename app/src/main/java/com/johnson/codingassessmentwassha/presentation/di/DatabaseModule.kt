package com.johnson.codingassessmentwassha.presentation.di

import android.content.Context
import androidx.room.Room
import com.johnson.codingassessmentwassha.data.db.JokeDatabase
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object DatabaseModule {

    @Provides
    @Singleton
    fun provideJokeDatabase(@ApplicationContext context: Context): JokeDatabase {
        return Room.databaseBuilder(
            context,
            JokeDatabase::class.java,
            "joke_database"
        ).build()
    }
}