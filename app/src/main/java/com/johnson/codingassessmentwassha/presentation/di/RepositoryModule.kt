package com.johnson.codingassessmentwassha.presentation.di

import com.johnson.codingassessmentwassha.data.db.JokeDao
import com.johnson.codingassessmentwassha.data.db.JokeDatabase
import com.johnson.codingassessmentwassha.data.repository.JokeRepositoryImpl
import com.johnson.codingassessmentwassha.data.repository.dataSource.JokeRemoteDataSource
import com.johnson.codingassessmentwassha.domain.repository.JokeRepository
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
class RepositoryModule {

    @Provides
    @Singleton
    fun provideJokeRepository(
        jokeRemoteDataSource: JokeRemoteDataSource,
        jokeDao: JokeDao
    ):JokeRepository{
        return JokeRepositoryImpl(jokeRemoteDataSource,jokeDao)
    }

    @Provides
    @Singleton
    fun provideJokeDao(database: JokeDatabase): JokeDao {
        return database.jokeDao() ?: throw IllegalStateException("JokeDao is null")
    }


}