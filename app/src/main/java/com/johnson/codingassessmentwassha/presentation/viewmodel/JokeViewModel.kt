package com.johnson.codingassessmentwassha.presentation.viewmodel

import android.app.Application
import android.content.Context
import android.net.ConnectivityManager
import android.net.NetworkCapabilities
import android.util.Log
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope

import com.johnson.codingassessmentwassha.data.model.APIResponse
import com.johnson.codingassessmentwassha.data.util.Resource
import com.johnson.codingassessmentwassha.domain.repository.JokeRepository
import com.johnson.codingassessmentwassha.domain.usecase.FetchJokeUseCase
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class JokeViewModel(
    private val app: Application,
    private val fetchJokeUseCase: FetchJokeUseCase,
    private val jokeRepository: JokeRepository

):AndroidViewModel(app) {
    val joke: MutableLiveData<Resource<APIResponse>> = MutableLiveData()


    fun fetchJoke() = viewModelScope.launch(Dispatchers.IO) {
        try {
            if (isNetWorkAvailable(app)) {
                joke.postValue(Resource.Loading())
                val apiResult = fetchJokeUseCase.execute()
                if (apiResult is Resource.Success) {
                    apiResult.data?.let {
                        // Save the converted JokeEntity to the local database
                        jokeRepository.saveJoke(it)
                    }
                }
                joke.postValue(apiResult)

            } else {
                val localJoke = jokeRepository.getLatestJoke()
                if (localJoke != null) {
                    joke.postValue(Resource.Error("Data was fetched from local", localJoke))
                } else {
                    joke.postValue(Resource.Error("Internet is not available"))
                }
            }
        } catch (e: Exception) {
            joke.postValue(Resource.Error(e.message.toString()))
        }
    }


    private fun isNetWorkAvailable(context: Context?):Boolean{
        if(context == null) return false
        val connectivityManager =
            context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager

        val network = connectivityManager.activeNetwork
        val networkCapabilities = connectivityManager.getNetworkCapabilities(network)

        return networkCapabilities?.run {
            hasTransport(NetworkCapabilities.TRANSPORT_CELLULAR) ||
                    hasTransport(NetworkCapabilities.TRANSPORT_WIFI) ||
                    hasTransport(NetworkCapabilities.TRANSPORT_ETHERNET)
        } ?: false
    }
}