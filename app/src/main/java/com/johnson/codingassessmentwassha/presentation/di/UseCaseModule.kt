package com.johnson.codingassessmentwassha.presentation.di

import com.johnson.codingassessmentwassha.domain.repository.JokeRepository
import com.johnson.codingassessmentwassha.domain.usecase.FetchJokeUseCase
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent

@Module
@InstallIn(SingletonComponent::class)
class UseCaseModule {
    @Provides
    fun provideJokeUseCase(
        jokeRepository: JokeRepository
    ):FetchJokeUseCase{
        return FetchJokeUseCase(jokeRepository)
    }
}