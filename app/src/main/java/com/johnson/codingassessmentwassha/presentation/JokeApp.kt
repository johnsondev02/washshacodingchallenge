package com.johnson.codingassessmentwassha.presentation

import android.app.Application
import dagger.hilt.android.HiltAndroidApp

@HiltAndroidApp
class JokeApp: Application()