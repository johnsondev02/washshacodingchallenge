package com.johnson.codingassessmentwassha.domain.usecase

import com.johnson.codingassessmentwassha.data.model.APIResponse
import com.johnson.codingassessmentwassha.data.util.Resource
import com.johnson.codingassessmentwassha.domain.repository.JokeRepository

class FetchJokeUseCase(private val jokeRepository: JokeRepository) {
    suspend fun execute():Resource<APIResponse>{
        return jokeRepository.fetchJokes()
    }
}