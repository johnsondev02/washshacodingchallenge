package com.johnson.codingassessmentwassha.domain.repository

import com.johnson.codingassessmentwassha.data.model.APIResponse
import com.johnson.codingassessmentwassha.data.util.Resource

interface JokeRepository {

    suspend fun fetchJokes():Resource<APIResponse>
    suspend fun saveJoke(joke: APIResponse)
    suspend fun getLatestJoke(): APIResponse?
}