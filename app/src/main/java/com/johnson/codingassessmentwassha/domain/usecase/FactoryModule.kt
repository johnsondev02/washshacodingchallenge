package com.johnson.codingassessmentwassha.domain.usecase

import android.app.Application
import com.johnson.codingassessmentwassha.domain.repository.JokeRepository
import com.johnson.codingassessmentwassha.presentation.viewmodel.JokeViewModelFactory
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
class FactoryModule {

    @Provides
    @Singleton
    fun provideJokeViewModelFactory(
        application: Application,
        fetchJokeUseCase: FetchJokeUseCase,
        jokeRepository: JokeRepository
    ):JokeViewModelFactory{
        return JokeViewModelFactory(
            application,
            fetchJokeUseCase,
            jokeRepository
        )
    }
}