package com.johnson.codingassessmentwassha.data.api

import kotlinx.coroutines.runBlocking
import okhttp3.mockwebserver.MockResponse
import okhttp3.mockwebserver.MockWebServer
import okio.buffer
import okio.source
import org.junit.After
import org.junit.Assert.assertArrayEquals
import org.junit.Assert.assertEquals
import org.junit.Assert.assertNotNull
import org.junit.Before
import org.junit.Test
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.nio.charset.Charset

class JokeApiServiceTest {
    private lateinit var service: JokeApiService
    private lateinit var server:MockWebServer

    @Before
    fun setUp() {
        server = MockWebServer()
        service = Retrofit.Builder()
            .baseUrl(server.url(""))
            .addConverterFactory(GsonConverterFactory.create())
            .build()
            .create(JokeApiService::class.java)
    }

    private fun enqueueMockResponse(
        filename:String
    ){
        val inputStream = javaClass.classLoader!!.getResourceAsStream(filename)
        val source = inputStream.source().buffer()
        val mockResponse = MockResponse()
        mockResponse.setBody(source.readString(Charsets.UTF_8))
        server.enqueue(mockResponse)
    }

    @Test
    fun fetchJoke_receivedExpected(){
        runBlocking {
            enqueueMockResponse("jokeresponse.json")
            val responseBody = service.getJoke().body()
            val request = server.takeRequest()
            assertNotNull("Response body should not be null",responseBody)
            assertEquals("/joke/Programming?type=single",request.path)
        }
    }

    @After
    fun tearDown() {
        server.shutdown()
    }
}